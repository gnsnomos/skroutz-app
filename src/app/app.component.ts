import { Component } from '@angular/core';
import { UserInfo } from 'src/app/user.info';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent {
    public title = 'skroutz-app';
    public user: UserInfo = null;

    public userSelected(user): void {
        this.user = user;
    }
}
