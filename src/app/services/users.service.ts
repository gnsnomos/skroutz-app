import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { UserInfo } from '../user.info';
import { map } from 'rxjs/operators';

export interface UserFormData {
    name: string;
    email: string;
    phone: string;
    address: string;
    company: string;
}

@Injectable()
export class UsersService {

    private _usersListJsonURL = './resources/user-data.json';
    private _cachedUsersList: UserInfo[];
    usersList: Observable<UserInfo[]>;

    constructor(private _http: HttpClient) {
    }

    public getUsersList(): Observable<UserInfo[]> {
        if (!this._cachedUsersList) {
            return this._http.get<UserInfo[]>(this._usersListJsonURL).pipe(
                map(list => { this._cachedUsersList = list; return list; })
            );
        } else {
            return of(this._cachedUsersList);
        }
    }

    public saveUser(id: string, userData: UserFormData) {
        if (this._cachedUsersList) {
            this._cachedUsersList.map(user => {
                if (user.id === id) {
                    user.name = userData.name;
                    user.email = userData.email;
                    user.phone = userData.phone;
                    user.address = userData.address;
                    user.company = userData.company;
                }
            });
        }
    }
}