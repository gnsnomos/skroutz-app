export interface UserInfo {
    id: string;
    photo: string;
    name: string;
    company: string;
    email: string;
    phone: string;
    address: string;
}