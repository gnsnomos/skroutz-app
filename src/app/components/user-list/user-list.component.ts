import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { UserInfo } from 'src/app/user.info';

@Component({
    selector: 'user-list',
    templateUrl: 'user-list.component.html',
    styleUrls: ['./user-list.component.scss']
})
/**
 * Shows a list with users
 * @class UserListComponent
 */
export class UserListComponent implements OnInit {

    public users: UserInfo[];
    public selectedUser: UserInfo = null;

    @Output() onUserSelected = new EventEmitter<UserInfo>();

    constructor(private _usersService: UsersService) { }

    public ngOnInit(): void {
        this._usersService.getUsersList().subscribe(list => {
            this.users = list;
        });
    }

    /**
     * Set user as selected and emits user data
     * @method select
     * @param user 
     */
    public select(user: UserInfo): void {
        this.selectedUser = user;
        this.onUserSelected.emit(user);
    }

}