import { Component, OnInit, Input, OnChanges, SimpleChanges } from '@angular/core';
import { UsersService } from 'src/app/services/users.service';
import { UserInfo } from 'src/app/user.info';
import { FormGroup, FormControl, FormBuilder } from '@angular/forms';

@Component({
    selector: 'user-data-form',
    templateUrl: 'user-data-form.component.html',
    styleUrls: ['./user-data-form.component.scss']
})
/**
 * Shows user's data
 * @class UserDataFormComponent
 */
export class UserDataFormComponent implements OnInit, OnChanges {
    @Input() public user: UserInfo;

    public userDataForm: FormGroup;
    public loadedUserData: UserInfo = null;

    constructor(private fb: FormBuilder, private _usersService: UsersService) { }

    public ngOnInit(): void {
        // @TODO: Validation
        this.userDataForm = this.fb.group({
            name: new FormControl(''),
            email: new FormControl(''),
            phone: new FormControl(''),
            address: new FormControl(''),
            company: new FormControl(''),
        });
    }

    public ngOnChanges(changes: SimpleChanges): void {
        if (changes.user && !changes.user.isFirstChange()) {
            this.loadedUserData = changes.user.currentValue;
            this.userDataForm.controls.name.setValue(changes.user.currentValue.name);
            this.userDataForm.controls.email.setValue(changes.user.currentValue.email);
            this.userDataForm.controls.phone.setValue(changes.user.currentValue.phone);
            this.userDataForm.controls.address.setValue(changes.user.currentValue.address);
            this.userDataForm.controls.company.setValue(changes.user.currentValue.company);
        }
    }

    public onSubmit(form: FormGroup) {
        if (form.valid) {
            this._usersService.saveUser(this.loadedUserData.id, form.value);
        }
        this.userDataForm.markAsPristine();
        this.userDataForm.markAsUntouched();
    }

    public onCancel() {
        if (this.loadedUserData) {
            this.userDataForm.controls.name.setValue(this.loadedUserData.name);
            this.userDataForm.controls.email.setValue(this.loadedUserData.email);
            this.userDataForm.controls.phone.setValue(this.loadedUserData.phone);
            this.userDataForm.controls.address.setValue(this.loadedUserData.address);
            this.userDataForm.controls.company.setValue(this.loadedUserData.company);
        }
        this.userDataForm.markAsPristine();
        this.userDataForm.markAsUntouched();
    }
}